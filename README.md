# express1

## Initial setup
1. Start up the ec2 instance with ssh entering your pem key and user
2. Clone master via git https: git clone --single-branch --branch master https://gitlab.com/Sal94/express1.git /home/ec2-user/express1
3. Clone testing via git https: git clone --single-branch --branch testing https://gitlab.com/Sal94/express1.git /home/ec2-user/express1-testing
3. Cd into express1: cd express1  
4. Copy nginx conf running: cp deploy/nginx-tls.conf ~/etc/nginx/conf.d
5. Copy nginx conf redirect: cp deploy/nginx-redirect.conf ~/etc/nginx/conf.d
6. Run container: deploy/express1.sh -d
7. Cd back to user folder: cd ~/express1-testing
8. Copy nginx conf running: cp deployFromTesting/nginx-tls-testing.conf ~/etc/nginx/conf.d
9. Copy nginx conf redirect: cp deployFromTesting/nginx-redirect-testing.conf ~/etc/nginx/conf.d
10. Run container: deployFromTesting/express1-testing.sh -d
11. Run nginx container: deployFromTesting/nginx.sh -d

